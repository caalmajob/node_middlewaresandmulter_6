const express = require('express');
const Pet = require('../models/Pets');
const { upload } = require('../middlewares/files.middleware');

const router = express.Router();

function getSpecieName(specie) {
    const species = {
    'dog': 'Perro',
    'cat': 'Gato',
    'bird': 'Pajaro'
    }
    return species[specie];
}

router.get('/', async (req, res, next) => {
    try {
    const pets = await Pet.find();
    return res.render('pets', { pets });
    } catch(error) {
        next(new Error(error));
    }
});

router.get('/create-pet', (req, res, next) => {
    return res.render('create-pet');
});

router.get('/:id', async (req, res, next) => {
    try {
    const { id } = req.params;
    const pet = await Pet.findById(id);
    if(pet) {
        pet.specie = getSpecieName(pet.specie);
        return res.render('pet', {pet});
    } else {
        return res.status(404).json("No pet found for this id")
    }
    } catch(error) {
        next(new Error(error));
    }
});

router.delete('/:id', async (req, res, next) => {
    try {
    const { id } = req.params;
    const deleted = await Pet.findByIdAndDelete(id);
    if(deleted) {
        return res.status(200).json('Pet deleted');
    }
        return res.status(200).json("Pet not found");
    } catch(error) {
        next(error);
    }
});

router.get('/species/:specie', async (req, res, next) => {
    try {
    const { specie } = req.params;
    const petsBySpecie = await Pet.find( {specie});
    return res.json(petsBySpecie);
    } catch(error) {
        next(new Error(error));
    }
});

router.get('/age/:age', async (req, res, next) => {
    try {
    const { age } = req.params;
    const petsByAge = await Pet.find( {age: {$gte: age}});
    return res.json(petsByAge);
    } catch(error) {
        next(new Error(error));
    }
});




router.post('/create', upload.single('avatar'), async (req, res, next) => {
    try {
        const { name, age, specie, color, breed } = req.body;
        let image;

        if(req.file) {
            image = req.file.filename;
        };

        const newPet = await new Pet({name, age, specie, color, breed, image});

        const createdPet = await newPet.save();

        return res.redirect('/pets');
    } catch (error) {
        next(error);
    }
});

module.exports = router;