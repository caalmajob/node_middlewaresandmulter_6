const express = require('express');
const passport = require('passport');

const router = express.Router();


router.get('/register', (req, res, next) => {
    return res.render('register');
});

router.post('/register', (req, res, next) => {
    const { email, password } = req.body;

    console.log('Registrando usuario...', req.body);

    if(!email || !password) {
        const error = new Error('User and password are required');
        return res.render('register', {error: error.message});
    }

    passport.authenticate('registro', (error, user) => {

        if(error) {
            return res.render('register', {error: error.message});
        }

        return res.redirect('/auth/login');
    })(req);
})

router.get('/login', (req, res, next) => {
    return res.render('login');
});

router.post('/login', (req, res, next) => {
    const { email, password } = req.body;
    console.log('Logueando usuario...', req.body);

    if(!email || !password) {
        const error = new Error('User and password are required')
        return res.render('login', {error: error.message});
    }

    passport.authenticate('acceso', (error, user) => {
        if(error) {
            return res.render('login', {error: error.message});
        }

        req.logIn(user, (error) => {
            if(error) {
                return res.render('login', {error: error.message});
            }

            return res.redirect('/');
        })
    })(req, res, next)
});


router.post('/logout', (req, res, next) => {
    if(req.user) {
        req.logOut();

        req.session.destroy(() => {
            res.clearCookie('connect.sid');
            return res.render('index', {message: 'Usuario deslogeado con éxito'});
        })
    } else {
        return res.redirect('/', {message: 'User not found'});
    }
});

module.exports = router;