const express = require('express');
const { isAuthenticated } = require('../middlewares/auth.middleware');
const Shelter = require('../models/Shelters');


const router = express.Router();


router.get('/', [isAuthenticated], (request, response) => {
    return response.render('shelters');
});

router.post('/create', [isAuthenticated], async (req, res, next) => {
    try {
        const { name, location } = req.body;

        const newShelter = new Shelter({ name, location });
        const savedShelter = await newShelter.save();

        return res.json(savedShelter);

    } catch(error) {
        next(error);
    }
});


router.put('/add-pet', [isAuthenticated], async (req, res, next) => {
    try {
        const { shelterId, petId } = req.body;

        const updatedShelter = await Shelter.findByIdAndUpdate(
            shelterId,
            { $push: { pets: petId } },
            { new: true }
        );

        return res.json(updatedShelter);

    } catch(error) {
        next(error);
    }
});

router.get('/:id', [isAuthenticated], async (req, res, next) => {
    try {
        const { id } = req.params;

        const shelter = await Shelter.findById(id).populate('pets')

        return res.json(shelter);
     
    } catch(error) {
        next(error);
    }
})


module.exports = router;