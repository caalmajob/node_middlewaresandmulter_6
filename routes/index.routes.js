const express = require('express');

const router = express.Router();


const myOwnMid = (req, res, next) => {
    console.log('ESTO ES UN MIDDLEWARE ejecutado antes de la ruta');

    req.pepito = req.user;
    req.tiago = 'Hola, me llamo Tiago';
    next();
}


router.get('/', [myOwnMid], (req, res) => {
    console.log('req.pepito', req.pepito);
    console.log('req.manuel', req.manuel);
    return res.render('index', { title: 'Mi servior', user: req.user});
});

module.exports = router;