const hbs = require("hbs");

//Comprobamos que mascota tiene más de cinco años
hbs.registerHelper("gte", (a, b, opts) => {
  if (a >= b) {
    return opts.fn(this);
  } else {
    return opts.inverse(this);
  }
});

hbs.registerHelper("uppercase", (str) => {
  return str.toUpperCase();
});
